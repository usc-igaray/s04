
# students = ['abe', 'bob', 'Carl', 'Ernest', 'Dawn']
# grades = [99, 88, 77, 66, 55]

# for index, student in enumerate(students):
#     print(f'The grade of {student} is {grades[index]}')
    
    
car = {
    'brand': 'Mazda',
    'model': 'CX-5',
    'year': '2023',
    'color': 'Silver',
}

print(f'I own a {car["brand"]} {car["model"]} and it was made in {car["year"]}')